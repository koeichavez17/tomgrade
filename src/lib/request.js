import axios from 'axios';

/* eslint-disable */
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
/* eslint-enable */

export default axios;