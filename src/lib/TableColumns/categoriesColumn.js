const categoriesColumn = [
  {
    headerName: "Name",
    field: "categoryName",
    sortable: true,
    filter: true,
    checkboxSelection: true
  },
  {
    headerName: "Description",
    field: "categoryDescription",
    sortable: true,
    filter: true
  },
  {
    headerName: "Slug",
    field: "categorySlug",
    sortable: true,
    filter: true
  },
  {
    headerName: "Parent Category",
    field: "parentCategory",
    sortable: true,
    filter: true
  },
];

export const productInputCategoryColumn = [
  {
    headerName: "Name",
    field: "categoryName",
    checkboxSelection: true
  },
  {
    headerName: "Parent Category",
    field: "parentCategory",
  },
];

export default categoriesColumn;
