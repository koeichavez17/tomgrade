const productTableColumns = [
  {
    headerName: "Type",
    field: "productType",
    sortable: true,
    filter: true,
    checkboxSelection: true
  },
  {
    headerName: "Name",
    field: "productName",
    sortable: true,
    filter: true
  },
  {
    headerName: "SKU",
    field: "productSku",
    sortable: true,
    filter: true
  },
];

export default productTableColumns;
