
export const API_URL = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/tomgrade-automation-sns';
export const CHECK_TOKEN_API_PATH = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/check-token';
export const CHECK_DOMAIN_NAMESILO = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/namesilo-api';
export const PURCHASE_DOMAIN_API_PATH = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/domain/purchase';
export const ADD_CLOUDFLARE_SUBDOMAIN_API_PATH = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/domain/cloudflare/add-dns';
export const CHECK_DNS_CLOUDFLARE_SUBDOMAIN_API_PATH = 'https://0jp1r45g1g.execute-api.eu-west-2.amazonaws.com/Dev/domain/cloudflare/check-dns';