
export const KEY_NAME = "TomGradeInstance"
export const SECURITY_GROUP_ID = "sg-09b439157a1388074"


export const AVAILABLE = 'AVAILABLE';
export const UNAVAILABLE = 'UNAVAILABLE';


export const FAILED_CODE = 400;
export const SUCCESS_CODE = 200;


export const SET_SUBDOMAIN = 'set_subdomain';
export const SET_DOMAIN = 'set_domain';
export const CLIENT_SET_DOMAIN = 'client_set_domain';