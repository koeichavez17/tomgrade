import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { authActions } from 'actions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { apiMessageCodes } from 'constants';

import {
  EcommerceInfoPage,
  MainFormPage,
  DonePage,
  ErrorTokenPage,
  WaitingPage
} from "containers";

export default (ComposedComponent) => {
  class RequiresAuth extends Component {

    static propTypes = {
      location: PropTypes.object,
      checkToken: PropTypes.func,
      messageCode: PropTypes.string,
      history: PropTypes.object
    }

    componentDidMount() {
      const values = queryString.parse(this.props.location.search);
      this.props.checkToken(values);
    }

    render() {
      const { messageCode } = this.props;
      switch(messageCode) {
        case apiMessageCodes.WAITING: 
          return <WaitingPage { ...this.props } />;
          break;
        case apiMessageCodes.ERROR_TOKEN: 
          return <ErrorTokenPage { ...this.props } />
          break;
        case apiMessageCodes.FORM_IS_SUBMITTED: 
          return <DonePage { ...this.props } />
          break;
        case apiMessageCodes.FORM_NOT_SUBMITTED: {
          const { location: { pathname }} = this.props.history;
          if (pathname === '/') return <MainFormPage { ...this.props } />;
          return <EcommerceInfoPage { ...this.props } />;
        }
        default:
          return <ComposedComponent { ...this.props } />;
      }
    }
  }

  const mapStateToProps = state => ({
    messageCode: state.auth.messageCode
  });

  const mapDispatchToProps = dispatch => 
    bindActionCreators(
      {
        checkToken: authActions.checkTokenAuth,
      },
      dispatch
    );

  return connect(mapStateToProps, mapDispatchToProps)(RequiresAuth);

}