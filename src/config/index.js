

import Store from './store';
import AppRouter from './routes';


export {
  Store,
  AppRouter
};