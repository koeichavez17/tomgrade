import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  MainFormPage,
  EcommerceInfoPage,
  DonePage,
  ErrorTokenPage,
  WaitingPage
} from "containers";
import { RequiresAuth } from "hoc";

const AppRouter = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={RequiresAuth(MainFormPage)} />
      <Route exact path="/done" component={DonePage} />
      <Route exact path="/waiting" component={WaitingPage} />
      <Route exact path="/error-token" component={ErrorTokenPage} />
      <Route
        exact
        path="/ecommerce-info"
        component={RequiresAuth(EcommerceInfoPage)}
      />
    </Switch>
  </Router>
);

export default AppRouter;
