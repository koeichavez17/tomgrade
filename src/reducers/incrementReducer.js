import { INCREMENT, DECREMENT } from "../actions/incrementActionTypes";
import { handleActions, combineActions } from "redux-actions";
import {
  incrementActions
} from "actions/incrementActions";
import * as actionTypes from "../actions/incrementActionTypes";

const initialState = {
  count: 0
};

const reducer = handleActions(
  {
    [actionTypes.INCREMENT](state, action) {
      return { ...state, count: state.count + 1 };
    },
    [actionTypes.DECREMENT](state, action) {
      return { ...state, count: state.count - 1 };
    },
  },
  initialState
);

export default reducer;
