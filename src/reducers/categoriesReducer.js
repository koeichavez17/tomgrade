import {
  productActionTypes
} from 'actions';
import { handleActions } from 'redux-actions'

const initialState = {
  categories: []
};

const categoriesReducer = handleActions(
  {
    [productActionTypes.ADD_CATEGORY]: (state, action) => {
      const categories = [...state.categories];
      categories.push(action.payload);
      return {...state, categories};
    },
    [productActionTypes.DELETE_CATEGORY]: (state, { payload: categories }) => {
      const selectedCategories = categories.map(cat => cat.categoryName);
      const newCategories = state.categories.filter(cat => !selectedCategories.includes(cat.categoryName))
      return {...state, categories: newCategories};
    }
  },
  initialState
);

export default categoriesReducer;
