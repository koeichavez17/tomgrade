import { handleActions } from 'redux-actions';
import { authActionTypes } from 'actions';

const initialState = {
  messageCode: "WAITING",
  tokenData: {}
};

const authReducer = handleActions(
  {
    [authActionTypes.UPDATE_TOKEN]: (state, action) => {
      console.log("UPDATE TOKEN HERE");
      console.log(action);
      return {...state, messageCode: action.payload.data.form_status, tokenData: action.payload.data};
    },
    [authActionTypes.UPDATE_DOMAIN]: (state, action) => {
      console.log("UPDATE DOMAIN HERE");
      console.log(action);
      const newTokenData = {...state.tokenData, domain: action.payload.domain};
      return {...state, tokenData: newTokenData};
    },
  },
  initialState
);


export default authReducer;