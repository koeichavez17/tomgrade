import {
  productActionTypes
} from 'actions';
import { handleActions } from 'redux-actions';

const initialState = {
  products: []
};

const productReducer = handleActions(
  {
    [productActionTypes.ADD_PRODUCT]: (state, action) => {
      const products = [...state.products];
      products.push(action.payload);
      return {...state, products};
    },
  },
  initialState
);

export default productReducer;