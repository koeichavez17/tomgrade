import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import categoriesReducer from './categoriesReducer';
import productReducer from './productReducer';
import imageReducer from './imageReducer';
import authReducer from './authReducer';

export default combineReducers({
  form: formReducer,
  categories: categoriesReducer,
  products: productReducer,
  images: imageReducer,
  auth: authReducer
});