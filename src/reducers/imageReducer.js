import { handleActions, combineActions } from "redux-actions";
import { imageActionTypes } from "actions";

const initialState = {
  logo: "",
  frontImage: "",
  backImage: "",
  productImage: "",
  categoryImage: ""
};
const imageReducer = handleActions(
  {
    [combineActions(
      ...Object.values(imageActionTypes)
    )]: (state, { payload: { name, file } }) => ({
      ...state,
      [name]: file
    })
  },
  initialState
);

export default imageReducer;
