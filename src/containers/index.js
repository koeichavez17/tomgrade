import MainFormPage from "./MainFormPage/MainFormPage";
import EcommerceInfoPage from "./EcommerceInfoPage/EcommerceInfoPage";
import DonePage from "./DonePage/DonePage";
import ErrorTokenPage from "./ErrorToken/ErrorTokenPage";
import WaitingPage from "./WaitingPage/WaitingPage";

export { default as App } from "./App";
export {
  MainFormPage,
  EcommerceInfoPage,
  DonePage,
  ErrorTokenPage,
  WaitingPage
};
