import React, { Component } from "react";
import PropTypes from "prop-types";
import { TableComponent } from "components";
import { ProductTableColumns } from "lib";

import ProductPageForm from "./ProductComponents/ProductPageForm";

const ProductPage = props => (
  <React.Fragment>
    <h3>Product</h3>
    <br />
    <h5>Add new Product</h5>
    <br />

    <ProductPageForm
      updateGridApiRef={props.updateGridApiRef}
      categories={props.categories}
      products={props.productState.products}
      onSubmit={props.addProductHandler}
      addProductImage={props.addProductImage}
    />

    <br />

    <TableComponent
      height="500px"
      width="45%"
      columnDefs={ProductTableColumns}
      rowData={props.productState.products}
    />
  </React.Fragment>
);

ProductPage.propTypes = {
  addProductHandler: PropTypes.func,
  productState: PropTypes.object,
  categories: PropTypes.array,
  updateGridApiRef: PropTypes.func,
  addProductImage: PropTypes.func
};

export default ProductPage;
