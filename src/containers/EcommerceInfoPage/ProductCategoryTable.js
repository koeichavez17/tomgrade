import React, { Component } from "react";
import { AgGridReact } from "ag-grid-react";
import { CategoriesTableColumns } from "lib";
import { Button } from "components";
import PropTypes from "prop-types";

class ProductCategoryTable extends Component {
  onButtonClick = e => {
    // const selectedNodes = this.gridApi.getSelectedNodes();
    // const selectedData = selectedNodes.map(node => node.data);
    // console.log(selectedData);
    // console.log(`Selected nodes: ${selectedData}`);
    e.preventDefault();
  };

  onDeleteCategory = e => {
    const selectedNodes = this.gridApi.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    this.props.deleteCategoryHandler(selectedData);
  };

  render() {
    return (
      <div
        className="ag-theme-balham"
        style={{
          height: "500px",
          width: "100%"
        }}
      >
        <Button type="button" onClick={this.onDeleteCategory}>
          Delete
        </Button>
        <AgGridReact
          onGridReady={params => {
            this.gridApi = params.api; // eslint-disable-line
          }}
          rowSelection="multiple"
          columnDefs={CategoriesTableColumns}
          rowData={this.props.data}
        />
      </div>
    );
  }
}

ProductCategoryTable.propTypes = {
  deleteCategoryHandler: PropTypes.func,
  data: PropTypes.array
};

export default ProductCategoryTable;
