import React from "react";
import { ContainerReduxField } from "components";

const ShippingTab = () => (
  <React.Fragment>
    <ContainerReduxField
      colSize={5}
      label="Weight (kg)"
      name="productWeight"
      type="number"
      placeholder="Enter product weight"
    />
    <ContainerReduxField
      colSize={5}
      label="Dimensions Length"
      name="productDimensionsLength"
      type="number"
      placeholder="Enter dimensions length"
    />
    <ContainerReduxField
      colSize={5}
      label="Dimension Width"
      name="productDimensionWidth"
      type="number"
      placeholder="Enter dimensions width"
    />
    <ContainerReduxField
      colSize={5}
      label="Dimension Height"
      name="productDimensionHeight"
      type="number"
      placeholder="Enter dimension height"
    />
  </React.Fragment>
);

export default ShippingTab;
