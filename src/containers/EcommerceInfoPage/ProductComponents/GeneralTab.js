import React, { Component } from "react";
import PropTypes from "prop-types";
import { AgGridReact } from "ag-grid-react";
import { ContainerReduxField } from "components";
import { productInputCategoryColumn } from "lib";

class GeneralTab extends Component {
  onGridReady = params => {
    this.props.updateGridApiRef(params.api);
  }
  
  render() {
    return (
      <React.Fragment>
        <ContainerReduxField
          colSize={5}
          label="Regular price"
          name="regularPrice"
          type="number"
          placeholder="Enter regular price"
        />
        <ContainerReduxField
          colSize={5}
          label="Sale Price"
          name="salePrice"
          type="number"
          placeholder="Enter sale price"
        />
        <h5>Product Categories</h5>
        <div
          className="ag-theme-balham"
          style={{
            height: "200px",
            width: "30%"
          }}
        >
          <AgGridReact
            onGridReady={this.onGridReady}
            rowSelection="multiple"
            columnDefs={productInputCategoryColumn}
            rowData={this.props.categories}
          />
        </div>
      </React.Fragment>
    );
  }
}

GeneralTab.propTypes = {
  categories: PropTypes.array,
  updateGridApiRef: PropTypes.func
};

export default GeneralTab;
