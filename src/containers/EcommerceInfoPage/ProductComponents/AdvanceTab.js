import React from "react";
import { ContainerReduxField, ReduxBootCheck } from "components";

const AdvanceTab = () => (
  <React.Fragment>
    <ContainerReduxField
      as="textarea"
      rows={3}
      colSize={5}
      label="Purchase note"
      name="productPurchaseNote"
      placeholder="Enter Purchase note"
    />
    <ContainerReduxField
      type="text"
      colSize={5}
      label="Menu order"
      name="productMenuOrder"
      placeholder="Enter menu order"
    />
    <ContainerReduxField
      rows={3}
      colSize={5}
      type="checkbox"
      label="Enable reviews"
      name="productEnableReviews"
      component={ReduxBootCheck}
    />
  </React.Fragment>
);

export default AdvanceTab;
