import React, { Component } from "react";
import { reduxForm } from "redux-form";
import { formNames } from "constants";
import { Tab, Col, Nav, Row, Form } from "react-bootstrap";
import { ContainerReduxField, Button, ImagePicker } from "components";
import PropTypes from 'prop-types';
import GeneralTab from "./GeneralTab";
import InventoryTab from "./InventoryTab";
import AdvanceTab from "./AdvanceTab";
import ShippingTab from "./ShippingTab";

class ProductPageForm extends Component {
  state = {
    productType: "simple",
    activeKey: "general"
  };

  onChangeProductType = ({ target: { value } }) => {
    if (value === "variable") this.setState({ activeKey: "inventory" });
    else this.setState({ activeKey: "general" });
    this.setState({ productType: value });
  };

  onSelectTab = key => this.setState({ activeKey: key });

  render() {
    const productTypeOptions = [
      <option key={1} value="simple">
        Simple Product
      </option>,
      <option key={2} value="variable">
        Variable Product
      </option>
    ];
    const { handleSubmit } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
        <ContainerReduxField
          onChange={this.onChangeProductType}
          colSize={2}
          label="Product Type"
          name="productType"
          as="select"
          selectOptions={productTypeOptions}
        />

        <ImagePicker
          label="Upload the picture of the product"
          message="Drag the picture of the product here"
          imageName="productImage"
          action={this.props.addProductImage} />

        <ContainerReduxField
          colSize={5}
          label="Name"
          name="productName"
          type="text"
          placeholder="Enter product name"
        />
        <br />
        <Tab.Container
          id="left-tabs-example"
          defaultActiveKey="general"
          activeKey={this.state.activeKey}
          onSelect={this.onSelectTab}
        >
          <Row>
            <Col sm={2} lg={2}>
              <Nav variant="pills" className="flex-column">
                {this.state.productType === "simple" && (
                  <Nav.Item>
                    <Nav.Link eventKey="general">General</Nav.Link>
                  </Nav.Item>
                )}
                <Nav.Item>
                  <Nav.Link eventKey="inventory">Inventory</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="shipping">Shipping</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="linkedProducts">Linked Products</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="attributes">Attributes</Nav.Link>
                </Nav.Item>
                {this.state.productType === "variable" && (
                  <Nav.Item>
                    <Nav.Link eventKey="variations">Variations</Nav.Link>
                  </Nav.Item>
                )}
                <Nav.Item>
                  <Nav.Link eventKey="advanced">Advanced</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>

            <Col sm={true} lg={true}>
              <Tab.Content>
                <Tab.Pane eventKey="general">
                  <GeneralTab
                    updateGridApiRef={this.props.updateGridApiRef}
                    categories={this.props.categories}/>
                </Tab.Pane>
                <Tab.Pane eventKey="inventory">
                  <InventoryTab />
                </Tab.Pane>
                <Tab.Pane eventKey="shipping">
                  <ShippingTab />
                </Tab.Pane>
                <Tab.Pane eventKey="linkedProducts">
                  <h1>LINKED PRODUCTS</h1>
                </Tab.Pane>
                <Tab.Pane eventKey="attributes">
                  <h1>ATTRIBUTES</h1>
                </Tab.Pane>
                <Tab.Pane eventKey="variations">
                  <h1>VARIATIONS</h1>
                </Tab.Pane>
                <Tab.Pane eventKey="advanced">
                  <AdvanceTab />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>

          <br />
          <Row>
            <Col sm={true}>
              <Button type="submit">Add Product</Button>
            </Col>
          </Row>
        </Tab.Container>
      </Form>
    );
  }
}

ProductPageForm.propTypes = {
  handleSubmit: PropTypes.func,
  categories: PropTypes.array,
  updateGridApiRef: PropTypes.func,
  addProductImage: PropTypes.func
};

export default reduxForm({
  form: formNames.PRODUCT_FORM,
  destroyOnUnmount: false,
  initialValues: {
    productType: "simple",
    productName: "",
    // General Tab
    regularPrice: "0",
    salePrice: "0",
    // Inventory Tab
    productSku: "",
    productManageStock: false,
    productStockStatus: "in-stock",
    productSoldIndividually: false,
    productInitialNumberInStock: 0,
    // Shipping Tab
    productWeight: 0,
    productDimensionsLength: 0,
    productDimensionWidth: 0,
    productDimensionHeight: 0,
    // Advance Tab
    productPurchaseNote: "",
    productMenuOrder: "",
    productEnableReviews: false
  }
})(ProductPageForm);
