import React from "react";
import { ContainerReduxField, ReduxBootCheck } from "components";

const stockStatusOptions = [
  <option key={0} value="in-stock">
    In stock
  </option>,
  <option key={1} value="out-of-stock">
    Out of stock
  </option>,
  <option key={2} value="on-backorder">
    On backorder
  </option>
];

const InventoryTab = () => (
  <React.Fragment>
    <ContainerReduxField
      colSize={5}
      label="SKU"
      name="productSku"
      type="text"
      placeholder="Enter product sku"
    />

    <ContainerReduxField
      colSize={5}
      label="Manage stock?"
      name="productManageStock"
      type="checkbox"
      component={ReduxBootCheck}
      message="Enable stock management at product level"
    />

    <ContainerReduxField
      colSize={5}
      label="Stock Status"
      name="productStockStatus"
      as="select"
      selectOptions={stockStatusOptions}
    />

    <ContainerReduxField
      colSize={5}
      label="Sold individually"
      name="productSoldIndividually"
      type="checkbox"
      component={ReduxBootCheck}
      message="Enable this to only allow one of this item to be bought in a single order"
    />

    <ContainerReduxField
      colSize={5}
      label="Initial number in stock"
      name="productInitialNumberInStock"
      type="number"
      placeholder="Enter initial number in stock"
    />
  </React.Fragment>
);

export default InventoryTab;
