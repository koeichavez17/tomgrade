import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import { Header, RouteContainer, Button } from "components";
import { connect } from "react-redux";
import { productActions, imageActions } from "actions";
import { bindActionCreators } from "redux";
import { defaultValues, apiPaths } from "constants";
import PropTypes from "prop-types";
import { axios } from "lib";
import CategoryPage from "./CategoryPage";
import ProductPage from "./ProductPage";

class EcommerceInfoPage extends Component {
  state = {
    productCategoryGridApiRef: null
  };

  handleLaunchWebsite = e => {
    console.log("HANDLE SUBMIT HERE IN ECOMMERCE INFO PAGE HAHAH");
    console.log(this.props);
    const {
      aboutForm: { values, initialValues },
      categories,
      productState: { products }
    } = this.props;
    const categoryData = JSON.stringify(categories);
    const productData = JSON.stringify(products);
    const { tokenData } = this.props.auth;

    const data = {
      basicInfo: {
        brandDescription: (values && values.brandDescription) || "",
        headerText: (values && values.headerText) || "",
        siteName: (values && values.siteName) || "",
        wpUserEmail: (values && values.wpUserEmail) || "",
        wpUser: (values && values.wpUser) || "",
        wpPassword: (values && values.wpPassword) || ""
      },
      keyName: defaultValues.KEY_NAME,
      securityGroupId: defaultValues.SECURITY_GROUP_ID,
      token: tokenData.token,
      tokenData: JSON.stringify(tokenData),
      categoryData,
      productData
    };

    console.log("DATA HERE HAHAHA");
    console.log(data);

    axios
      .post(apiPaths.API_URL, data)
      .then(resData => {
        /* eslint-disable */
        window.location = "https://tomgrade.com";
        /* eslint-enable */
      })
      .catch(err => {
        console.log(err);
      });
  };

  updateGridApiRef = values => {
    this.setState({
      productCategoryGridApiRef: values
    });
  };

  goBackToAboutpage = () => {
    this.props.history.goBack();
  };

  addCategoryHandler = values => {
    const newValues = { ...values, categoryImage: this.props.categoryImage };
    this.props.addCategory(newValues);
  };

  deleteCategoryHandler = categories => {
    this.props.deleteCategory(categories);
  };

  addProductHandler = values => {
    console.log(this.state);
    const selectedNodes = this.state.productCategoryGridApiRef.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);

    console.log("ADD PRODUCT HANDLER");
    console.log(selectedData);

    const newValues = {
      ...values,
      categories: selectedData,
      productImage: this.props.productImage
    };

    this.props.addProduct(newValues);
  };

  render() {
    return (
      <RouteContainer>
        <Header handleLaunchWebsite={this.handleLaunchWebsite} />
        <Button type="button" onClick={this.goBackToAboutpage}>
          Back
        </Button>
        <Tabs defaultActiveKey="products" id="uncontrolled-tab-example">
          <Tab eventKey="categories" title="Product Categories">
            <br />
            <CategoryPage
              addCategoryImage={this.props.addCategoryImage}
              categories={this.props.categories}
              addCategoryHandler={this.addCategoryHandler}
              deleteCategoryHandler={this.deleteCategoryHandler}
            />
          </Tab>
          <Tab eventKey="products" title="Products">
            <br />
            <ProductPage
              addProductImage={this.props.addProductImage}
              updateGridApiRef={this.updateGridApiRef}
              categories={this.props.categories}
              addProductHandler={this.addProductHandler}
              productState={this.props.productState}
            />
          </Tab>
        </Tabs>
      </RouteContainer>
    );
  }
}

EcommerceInfoPage.propTypes = {
  history: PropTypes.object,
  addCategory: PropTypes.func,
  deleteCategory: PropTypes.func,
  addProduct: PropTypes.func,
  categories: PropTypes.array,
  productState: PropTypes.object,
  addProductImage: PropTypes.func,
  productImage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  addCategoryImage: PropTypes.func,
  categoryImage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  // forms
  aboutForm: PropTypes.object,

  // reducers
  auth: PropTypes.object,
};

const mapStateToProps = state => ({
  form: state.form.categoryForm,
  aboutForm: state.form.aboutForm,
  categories: state.categories.categories,
  productState: state.products,
  productImage: state.images.productImage,
  categoryImage: state.images.categoryImage,
  auth: state.auth
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addCategory: productActions.addCategory,
      deleteCategory: productActions.deleeteCategory,
      addProduct: productActions.addProduct,
      addProductImage: imageActions.addProductImage,
      addCategoryImage: imageActions.addCategoryImage
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EcommerceInfoPage);
