import React from "react";
import { Row, Col } from "react-bootstrap";
import PropTypes from 'prop-types'
import ProductCategoryForm from "./ProductCategoryForm";
import ProductCategoryTable from "./ProductCategoryTable";


const CategoryPage = props => (
  <React.Fragment>
    <h3>Product Categories</h3>
    <br />
    <h5>Add new Category</h5>
    <br />
    <Row>
      <Col lg={3} sm={3}>
        <ProductCategoryForm
          addCategoryImage={props.addCategoryImage}
          categories={props.categories}
          onSubmit={props.addCategoryHandler}
        />
      </Col>
      <Col lg={true} sm={true}>
        <ProductCategoryTable
          data={props.categories}
          deleteCategoryHandler={props.deleteCategoryHandler}
        />
      </Col>
    </Row>
  </React.Fragment>
);

CategoryPage.propTypes = {
  categories: PropTypes.array,
  addCategoryHandler: PropTypes.func,
  deleteCategoryHandler: PropTypes.func,
  addCategoryImage: PropTypes.func
};

export default CategoryPage;
