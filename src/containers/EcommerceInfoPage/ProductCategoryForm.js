import React, { Component } from "react";
import { reduxForm, reset } from "redux-form";
import { formNames } from "constants";
import { Form } from "react-bootstrap";
import { ContainerReduxField, Button, ImagePicker } from "components";
import PropTypes from "prop-types";

class ProductCategoryForm extends Component {
  checkIfCategoryIsValid = categoryName => {
    const data = this.props.categories.filter(
      cat => cat.categoryName === categoryName
    );
    return data.length > 0;
  };

  render() {
    const selectOptions = this.props.categories.map((cat, index) => (
      <option key={index + 1} value={cat.categoryName}>
        {cat.categoryName}
      </option>
    ));
    selectOptions.unshift(
      <option key={0} value="None">
        None
      </option>
    );
    const { handleSubmit } = this.props;
    const validateName = value =>
      this.checkIfCategoryIsValid(value)
        ? "Category name already existed"
        : undefined;
    // const validateName = value => this.checkIfCategoryIsValid(value);
    return (
      <Form onSubmit={handleSubmit}>
        <ContainerReduxField
          colSize={true}
          label="Name"
          name="categoryName"
          // validate={validateName}
          type="text"
          placeholder="Enter category name"
          message="The name is how it appears on your site."
        />

        <br />
        <ContainerReduxField
          colSize={true}
          label="Slug"
          name="categorySlug"
          type="text"
          placeholder="Enter category slug"
          message="The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens."
        />
        <br />
        <ContainerReduxField
          colSize={true}
          rows={4}
          label="Description"
          name="categoryDescription"
          as="textarea"
          placeholder="Enter category description"
          message="The description is not prominent by default; however, some themes may show it."
        />

        <br />

        <ContainerReduxField
          as="select"
          colSize={true}
          label="Parent Category"
          name="parentCategory"
          message="Assign a parent term to create a hierarchy. The term Jazz, for example, would be the parent of Bebop and Big Band."
          selectOptions={selectOptions}
        />
        <br />
        <ImagePicker
          label="Upload your category thumbnail"
          message="Drag the picture of the category thumbnail"
          imageName="categoryImage"
          action={this.props.addCategoryImage}
        />

        <br />
        <Button type="submit">Add new category</Button>
      </Form>
    );
  }
}

ProductCategoryForm.propTypes = {
  categories: PropTypes.array,
  data: PropTypes.array,
  handleSubmit: PropTypes.func,
  addCategoryImage: PropTypes.func
};

const resetForm = (result, dispatch) => {
  dispatch(reset("categoryForm"));
};

export default reduxForm({
  form: formNames.CATEGORY_FORM,
  destroyOnUnmount: false,
  onSubmitSuccess: resetForm,
  initialValues: {
    parentCategory: "None"
  }
})(ProductCategoryForm);
