import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  ContainerReduxField,
  ImagePicker
} from "components";

import { Form } from "react-bootstrap";
import { reduxForm } from "redux-form";
import { formNames, defaultValues } from "constants";


const AboutForm = ({ 
  onNextPage, 
  imageActions, 
  checkDomainAvailability, 
  checkSubdomainAvailability,
  purchaseDomain, 
  purchaseSubDomain,
  checkDomainStatus, 
  purchaseButtonState, 
  checkDomainButtonState, 
  domain,
  domainOptions,
}) => (
  <Form>
    {!domain && (
      <div>
        {domainOptions === defaultValues.SET_DOMAIN && (
          <div>
            <ContainerReduxField
              name="selectedDomain"
              label="Domain"
              type="text"
              placeholder="Enter the domain you want for your website"
            />
            <p>{checkDomainStatus}</p>
            <Button  
              type="button"
              disabled={checkDomainButtonState}
              onClick={checkDomainAvailability}>
              Check Domain
            </Button>
            <Button 
              type="button" 
              disabled={purchaseButtonState} 
              onClick={purchaseDomain}>
              Choose Domain
            </Button> 
          </div>
        )}

        {domainOptions === defaultValues.SET_SUBDOMAIN && (
          <div>
            <ContainerReduxField
              name="selectedDomain"
              label="Domain"
              type="text"
              placeholder="Enter the subdomain you want for your website."
            />
            <p>{checkDomainStatus}</p>
            <Button  
              type="button"
              disabled={checkDomainButtonState}
              onClick={checkSubdomainAvailability}>
              Check Subdomain
            </Button>
            <Button 
              type="button" 
              disabled={purchaseButtonState} 
              onClick={purchaseSubDomain}>
              Choose Subdomain
            </Button> 
          </div>
        )}
        
      </div>
    ) || (
      <div>
        <p>Domain: {domain}</p>
      </div>
    )}

    <br />

    <ContainerReduxField
      colSize={true}
      rows={8}
      name="brandDescription"
      as="textarea"
      type="textarea"
      placeholder="Enter brand description"
    />
    <br />

    <ImagePicker
      imageName="logo"
      label="Upload your logo"
      message="Drag your logo"
      action={imageActions.addLogo}
    />
    <ImagePicker
      imageName="frontImage"
      label="Upload your front image for the header"
      message="Drag your front image for the header"
      action={imageActions.addFrontImage}
    />
    <ImagePicker
      imageName="backImage"
      label="Upload your background image for the header"
      message="Drag your background image for the header"
      action={imageActions.addBackgroundImage}
    />

    <ContainerReduxField
      name="headerText"
      label="Header Text"
      type="text"
      placeholder="Enter the text you want for your header"
    />

    <ContainerReduxField
      name="siteName"
      label="Site Name"
      type="text"
      placeholder="Enter Site Name"
    />
    <br />
    <ContainerReduxField
      name="wpUser"
      label="Wordpress Username"
      type="text"
      placeholder="Enter Wordpress Username"
    />
    <br />
    <ContainerReduxField
      name="wpPassword"
      label="Wordpress Password"
      type="password"
      placeholder="Enter Wordpress Password"
    />
    <br />
    <ContainerReduxField
      name="wpUserEmail"
      label="Wordpress Email"
      type="email"
      placeholder="Enter Wordpress email"
    />
    <br />
    <Button type="button" onClick={onNextPage}>
      Next
    </Button>
  </Form>
);

AboutForm.propTypes = {
  onNextPage: PropTypes.func,
  imageActions: PropTypes.object,
  checkDomainAvailability: PropTypes.func,
  checkSubdomainAvailability: PropTypes.func,
  purchaseDomain: PropTypes.func,
  purchaseSubDomain: PropTypes.func,
  checkDomainStatus: PropTypes.string,
  purchaseButtonState: PropTypes.bool,
  checkDomainButtonState: PropTypes.bool,
  domain: PropTypes.string,
  domainOptions: PropTypes.string
};

export default reduxForm({
  form: formNames.ABOUT_FORM,
  destroyOnUnmount: false,
  initialValues: {
    brandDescription: "",
    headerText: "",
    siteName: "",
    wpUser: "",
    wpPassword: "",
    wpUserEmail: "",
    selectedDomain: ""
  }
})(AboutForm);
