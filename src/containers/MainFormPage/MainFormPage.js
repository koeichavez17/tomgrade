import React, { Component } from "react";
import { Paragraph, Header, RouteContainer } from "components";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { axios } from 'lib';
import { imageActions, authActions } from "actions";
import { defaultValues, apiPaths } from 'constants';
import AboutForm from "./AboutForm";


class MainFormPage extends Component {

  state = {
    purchaseButtonState: true,
    checkDomainButtonState: false,
    checkDomainStatus: this.props.domainOptions === defaultValues.SET_DOMAIN ? 'Search for domain' : 'Search for subdomain',
    domainFieldVisibility: false,
  };


  handleLaunchWebsite = (e) => {
    console.log("HANDLE SUBMIT HERE HAHAH");
    console.log(this.props);
  };

  goToEcommercePage = () => {
    console.log(this.props);
    this.props.history.push({
      pathname: '/ecommerce-info',
      search: this.props.location.search,
    })
  };

  checkDomainAvailability = () => {
    this.setState({ checkDomainStatus: 'Checking Domain ...', purchaseButtonState: true }, () => {
      const checkDomainUrl = apiPaths.CHECK_DOMAIN_NAMESILO;
      const params = {
        domain: this.props.aboutFormState.values.selectedDomain
      };
      axios.post(checkDomainUrl, params)
        .then(({data: { response }}) => {
          const newState = response === defaultValues.AVAILABLE ? 'Domain available' : 'Domain not available';
          const newPurchaseButtonState = !(response === defaultValues.AVAILABLE);
          // this.setState({ checkDomainStatus: newState, purchaseButtonState: newPurchaseButtonState, domainFieldVisibility: false });
          this.setState({ checkDomainStatus: newState, purchaseButtonState: newPurchaseButtonState });
        }).catch(err => {
          this.setState({ checkDomainStatus: 'Search for domain' });
          console.log("ERROR HERE IN CHECK DOMAIN AVAILABILITY");
          console.log(err);
        });
    });
  }

  checkSubdomainAvailability = async () => {
    this.setState({ checkDomainStatus: 'Checking Subdomain ...', purchaseButtonState: true }, () => {
      const subdomain = `${this.props.aboutFormState.values.selectedDomain}.tomgrade.com`;
      const checkSubDomainUrl = apiPaths.CHECK_DNS_CLOUDFLARE_SUBDOMAIN_API_PATH;
      const params = { subdomain };
      axios.post(checkSubDomainUrl, params)
        .then(({data: { code }}) => {
          console.log("SUCCESS HERE IN CHECK SUBDOMAIN AVAILABILITY");
          console.log(code);
          const newState = code === defaultValues.SUCCESS_CODE ? 'Subdomain Available' : 'Subdomain not available';
          const newPurchaseButtonState = !(code === defaultValues.SUCCESS_CODE);
          this.setState({ checkDomainStatus: newState, purchaseButtonState: newPurchaseButtonState });
        }).catch(err => {
          this.setState({ checkDomainStatus: 'Search for Subdomain'});
          console.log("ERROR HERE IN CHECK SUBDOMAIN AVAILABILITY");
          console.log(err);
        })
    });
  }

  purchaseDomain = () => {
    console.log("PURCHASE DOMAIN HERE");
    this.setState({ checkDomainStatus: 'Processing Domain ...', purchaseButtonState: true, checkDomainButtonState: true }, () => {
      const purchaseDomainUrl = apiPaths.PURCHASE_DOMAIN_API_PATH;
      const params = {
        token: this.props.token,
        domain: this.props.aboutFormState.values.selectedDomain
      };
      console.log(params);
      axios.post(purchaseDomainUrl, params)
        .then(({data}) => {
          this.props.updateDomain({ domain: params.domain });
        }).catch(err => {
          console.log("ERROR HERE IN PURCHASE DOMAIN AVAILABILITY");
          console.log(err);
        });
    });
    
  }

  purchaseSubDomain = () => {
    console.log("PURCHASE SUBDOMAIN HERE");
    this.setState({ checkDomainStatus: 'Processing Subdomain ...', purchaseButtonState: true, checkDomainButtonState: true }, () => {
      const purchaseSubDomainUrl = apiPaths.ADD_CLOUDFLARE_SUBDOMAIN_API_PATH;
      const params = {
        token: this.props.token,
        subdomain: this.props.aboutFormState.values.selectedDomain
      };
      console.log(params);
      axios.post(purchaseSubDomainUrl, params)
        .then(({data}) => {
          console.log("SUCCESS HERE IN PURCHASE SUBDOMAIN AVAILABILITY");
          console.log(data);
          this.props.updateDomain({ domain: `${params.subdomain}.tomgrade.com` });
        }).catch(err => {
          console.log("ERROR HERE IN PURCHASE SUBDOMAIN AVAILABILITY");
          console.log(err);
        });
    });
    
  }

  render() {
    return (
      <RouteContainer>
        <Header />
        <h3>Site Information</h3>
        <br />
        <Paragraph>
          Tell us about your brand! Just write a few lines about your
          business so we get to know you and understand what will work best for
          your website! We’d love to know about your products, target audience
          and anything else you think’s important to mention.
        </Paragraph>
        <AboutForm
          imageActions={{
            addLogo: this.props.addLogo,
            addFrontImage: this.props.addFrontImage,
            addBackgroundImage: this.props.addBackgroundImage
          }}
          formState={this.props.aboutFormState}
          onNextPage={this.goToEcommercePage}
          checkDomainAvailability={this.checkDomainAvailability}
          checkSubdomainAvailability={this.checkSubdomainAvailability}
          purchaseDomain={this.purchaseDomain}
          purchaseSubDomain={this.purchaseSubDomain}
          purchaseButtonState={this.state.purchaseButtonState}
          checkDomainButtonState={this.state.checkDomainButtonState}
          checkDomainStatus={this.state.checkDomainStatus}
          domain={this.props.domain}
          domainOptions={this.props.domainOptions}
        />
      </RouteContainer>
    );
  }
}

MainFormPage.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  // Dispatch Actions
  addLogo: PropTypes.func,
  addFrontImage: PropTypes.func,
  addBackgroundImage: PropTypes.func,
  updateDomain: PropTypes.func,
  // State Props
  aboutFormState: PropTypes.object,
  token: PropTypes.string,
  domain: PropTypes.string,
  domainOptions: PropTypes.string
};

const mapStateToProps = state => ({
  aboutFormState: state.form.aboutForm,
  token: state.auth.tokenData.token,
  domain: state.auth.tokenData.domain,
  domainOptions: state.auth.tokenData.domain_options
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addLogo: imageActions.addLogo,
      addFrontImage: imageActions.addFrontImage,
      addBackgroundImage: imageActions.addBackgroundImage,
      updateDomain: authActions.updateDomainAuth,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainFormPage);
