
// PRODUCT CATEGORY RELATED
export const ADD_CATEGORY = "ADD [category]"
export const UPDATE_CATEGORY = "UPDATE [category]"
export const DELETE_CATEGORY = "DELETE [category]"

export const ADD_PRODUCT = "ADD [product]"
export const UPDATE_PRODUCT = "UPDATE [product]"
export const DELETE_PRODUCT = "DELETE [product]"
