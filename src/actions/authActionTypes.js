

export const CHECK_TOKEN = 'CHECK TOKEN [auth]';
export const UPDATE_TOKEN = 'UPDATE TOKEN [auth]';
export const FINISHED_CHECKING_TOKEN = 'FINISHED CHECKING TOKEN [auth]';

export const UPDATE_DOMAIN = 'UPDATE DOMAIN [auth]';
