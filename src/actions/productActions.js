import { createActions } from 'redux-actions';
import * as actionTypes from './productActionTypes';

const productActions = createActions(
  {},
  ...Object.values(actionTypes) 
);

export default productActions;