import { createActions } from 'redux-actions';
import * as actionTypes from './authActionTypes';


const authActions = createActions(
  {},
  ...Object.values(actionTypes)
);

export default authActions;