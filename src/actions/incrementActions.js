import { createActions } from "redux-actions";
import * as actionTypes from './incrementActionTypes';


export const incrementActions = createActions(
  {},
  ...Object.values(actionTypes)
);
