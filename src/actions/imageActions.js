import { createActions } from "redux-actions";
import * as imageActionTypes from "./imageActionsTypes";

const imageActions = createActions({}, ...Object.values(imageActionTypes));

export default imageActions;
