import React from "react";
import { Button } from "react-bootstrap";
import PropTypes from "prop-types";

const ButtonComponent = ({ type, children, onClick, className, disabled }) => (
  <Button onClick={onClick} type={type} disabled={disabled}>
    {children}
  </Button>
);

ButtonComponent.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

export default ButtonComponent;
