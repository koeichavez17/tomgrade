import React, { useState, useEffect, useCallback } from "react";
import Dropzone, { useDropzone } from "react-dropzone";
import styled from "styled-components";
import PropTypes from "prop-types";
import { connect } from 'react-redux';

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: "border-box"
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden"
};

const img = {
  display: "block",
  width: "auto",
  height: "100%"
};

const getColor = props => {
  if (props.isDragAccept) {
    return "#00e676";
  }
  if (props.isDragReject) {
    return "#ff1744";
  }
  if (props.isDragActive) {
    return "#6c6";
  }
  return "#666";
};

const Container = styled.div`
  width: 200px;
  height: 200px;
  border-width: 2px;
  border-radius: 5px;
  border-color: ${props => getColor(props)};
  border-style: ${props => (props.isDragActive ? "solid" : "dashed")};
  background-color: ${props => (props.isDragActive ? "#eee" : "")};
`;

const CustomDropzone = ({message, label, action, imageName, images}) => {
  const [files, setFiles] = useState([]);
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({
    accept: "image/*",
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file => {
          const data = Object.assign(file, {
            preview: URL.createObjectURL(file)
          });
          action({name: imageName, file: data});
          return data;
        })
      );
    }
  });

  const thumbs = files.map(file => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <Container {...getRootProps({ isDragActive, isDragAccept, isDragReject })}>
      <input {...getInputProps()} />
      <p>{message}</p>
      <aside style={thumbsContainer}>{thumbs}</aside>
    </Container>
  );
};

CustomDropzone.propTypes = {
  label: PropTypes.string,
  message: PropTypes.string,
  action: PropTypes.func,
  imageName: PropTypes.string,
  images: PropTypes.object
};

const mapStateToProps = state => ({
  images: state.images
});

export default connect(mapStateToProps)(CustomDropzone);
