import React from 'react';
import { Container } from 'react-bootstrap';
import PropTypes from 'prop-types';

const RouteContainer = ({ children }) => (
  <React.Fragment>
    <Container fluid>
      {children}
    </Container>
  </React.Fragment>
);

RouteContainer.propTypes = {
  children: PropTypes.node
};

export default RouteContainer;