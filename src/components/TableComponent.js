import React, { Component } from "react";
import PropTypes from "prop-types";
import { AgGridReact } from 'ag-grid-react';

class TableComponent extends Component {
  render() {
    return (
      <div
        className="ag-theme-balham"
        style={{
          height: this.props.height,
          width: this.props.width
        }}
      >
        <AgGridReact
          onGridReady={params => {
            this.gridApi = params.api; // eslint-disable-line
          }}
          rowSelection="multiple"
          columnDefs={this.props.columnDefs}
          rowData={this.props.rowData}
        />
      </div>
    );
  }
}

TableComponent.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  columnDefs: PropTypes.array,
  rowData: PropTypes.array
};

export default TableComponent;
