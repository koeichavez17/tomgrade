import React from "react";
import { CustomDropzone } from "components";
import PropTypes from 'prop-types'

const ImagePicker = ({
  label,
  action,
  message,
  imageName
}) => (
  <React.Fragment>
    <label>{label}</label>
    <CustomDropzone
      imageName={imageName}
      action={action}
      message={message}
    />
    <br />
  </React.Fragment>
);

ImagePicker.propTypes = {
  label: PropTypes.string,
  message: PropTypes.string,
  action: PropTypes.func,
  imageName: PropTypes.string
}


export default ImagePicker;
