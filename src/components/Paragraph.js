import React from 'react';
import PropTypes from 'prop-types';


const Paragraph = (props) => (
  <p>
    {props.children}
  </p>
);

Paragraph.propTypes = {
  children: PropTypes.node.isRequired
};

export default Paragraph;