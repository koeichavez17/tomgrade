import React from "react";
import PropTypes from "prop-types";
import { Form } from "react-bootstrap";
import { Field } from "redux-form";
import { Container, ReduxBootField } from ".";

const ContainerReduxField = ({
  as,
  label,
  type,
  placeholder,
  name,
  component,
  value,
  colSize = 3,
  rows = 3,
  message,
  selectOptions,
  validate,
  meta,
  onChange
}) => (
  <Container colSize={colSize}>
    {!component && <Form.Label>{label}</Form.Label>}
    <div>
      {meta && meta.touched &&
        ((meta.error && <span>{meta.error}</span>) ||
          (meta.warning && <span>{meta.warning}</span>))}
    </div>
    <Field
      label={label}
      validate={validate}
      rows={rows}
      value={value}
      as={as}
      type={type}
      placeholder={placeholder}
      name={name}
      component={component || ReduxBootField}
      onChange={onChange}
    >
      {selectOptions}
    </Field>
    <Form.Text className="text-muted">{message}</Form.Text>
  </Container>
);

ContainerReduxField.propTypes = {
  as: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  component: PropTypes.func,
  value: PropTypes.string,
  colSize: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  rows: PropTypes.number,
  message: PropTypes.string,
  selectOptions: PropTypes.arrayOf(PropTypes.node),
  validate: PropTypes.func,
  onChange: PropTypes.func,
  meta: PropTypes.object
};

export default ContainerReduxField;
