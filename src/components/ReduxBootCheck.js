import React from "react";
import PropTypes from "prop-types";
import { Form } from "react-bootstrap";

const ReduxBootCheck = ({ input, name, label }) => (
  <React.Fragment>
    <Form.Check
      name={name}
      label={label}
      checked={input.value || false}
      onChange={(e) => input.onChange(e.target.checked)}
    />
  </React.Fragment>
);

ReduxBootCheck.propTypes = {
  label: PropTypes.string,
  input: PropTypes.object,
  name: PropTypes.string,
};

export default ReduxBootCheck;
