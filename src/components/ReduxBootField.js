import React from "react";
import PropTypes from "prop-types";
import { FormControl } from "react-bootstrap";

const ReduxBootField = props => (
  <FormControl
    {...props}
    value={props.input.value}
    onChange={props.onChange || props.input.onChange}
  />
);

ReduxBootField.propTypes = {
  input: PropTypes.object,
  onChange: PropTypes.func
};

export default ReduxBootField;
