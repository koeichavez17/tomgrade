import React from "react";
import { Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";

const ContainerComponent = props => (
  <Row>
    <Col lg={props.colSize} sm={props.colSize}>
      {props.children}
    </Col>
  </Row>
);

ContainerComponent.propTypes = {
  colSize: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool
  ]),
  children: PropTypes.node.isRequired
};

export default ContainerComponent;
