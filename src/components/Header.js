import React from "react";
import PropTypes from "prop-types";
import { Paragraph, Button } from ".";

const Header = ({ handleLaunchWebsite }) => (
  <React.Fragment>
    <h1>Tom Grade Launcher Form</h1>
    <hr />
    {handleLaunchWebsite && (
      <Button type="button" onClick={handleLaunchWebsite}>
        Launch Website
      </Button>
    )}
    <Paragraph>
      IMPORTANT! You need to fill out the website launch form before we can
      create your website.
    </Paragraph>
  </React.Fragment>
);

Header.propTypes = {
  handleLaunchWebsite: PropTypes.func
};

export default Header;
