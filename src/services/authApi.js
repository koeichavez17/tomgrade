import { axios } from "lib";
import { apiPaths } from "constants";

export const getToken = token => axios.post(`${apiPaths.CHECK_TOKEN_API_PATH}`, { token }).then(({data}) => data);
