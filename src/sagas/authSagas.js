import { takeEvery, call, put } from "redux-saga/effects";
import { authApi } from "services";
import { authActions } from "actions";
import { CHECK_TOKEN } from "../actions/authActionTypes";

function* getTokenData(action) {
  try {
    const { payload: { token } } = action;
    const tokenData = yield call(authApi.getToken, token);
    yield put(authActions.updateTokenAuth(tokenData));
    yield put(authActions.finishedCheckingTokenAuth());
  } catch (err) {
    console.log("Error here in getTokenData", err);
  }
}

function* authSagas() {
  yield takeEvery(CHECK_TOKEN, getTokenData);
}

export default authSagas;
