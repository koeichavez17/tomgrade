module.exports = {
  plugins: ['react', 'flowtype', 'fp'],
  extends: [
      'airbnb-base',
      'react-app',
      'plugin:fp/recommended',
      'plugin:react/recommended',
      'plugin:flowtype/recommended',
      'prettier',
  ],
  rules: {
      'import/no-unresolved': 0,
      'import/prefer-default-export': 0,
      'no-console': 0,
      'fp/no-mutating-methods': 0,
      'fp/no-nil': 0,
      'fp/no-unused-expression': 0,
      'fp/no-mutating-assign': 0,
      'fp/no-class': 0,
      'fp/no-this': 0,
      'fp/no-mutation': [
          'error',
          {
              exceptions: [
                  { property: 'propTypes' },
                  { property: 'defaultProps' },
                  { property: 'exports' },
              ],
          },
      ],
  },
  settings: {
      react: {
          version: 'latest',
      },
  },
};
