const path = require('path');

const sharedConfig = {
  alias: {
    'react-native': 'react-native-web',
    'actions': path.resolve(__dirname, '../src/actions'),
    'assets': path.resolve(__dirname, '../src/assets'),
    'components': path.resolve(__dirname, '../src/components'),
    'config': path.resolve(__dirname, '../src/config'),
    'constants': path.resolve(__dirname, '../src/constants'),
    'containers': path.resolve(__dirname, '../src/containers'),
    'context': path.resolve(__dirname, '../src/context'),
    'data': path.resolve(__dirname, '../src/data'),
    'hoc': path.resolve(__dirname, '../src/hoc'),
    'lib': path.resolve(__dirname, '../src/lib'),
    'reducers': path.resolve(__dirname, '../src/reducers'),
    'sagas': path.resolve(__dirname, '../src/sagas'),
    'scenes': path.resolve(__dirname, '../src/scenes'),
    'services': path.resolve(__dirname, '../src/services'),
    'styles': path.resolve(__dirname, '../src/styles'),
  },
};

module.exports = sharedConfig;